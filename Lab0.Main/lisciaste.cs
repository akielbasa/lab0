﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class lisciaste : drzewo
    {
        string czyMajaOwoce;
        public string CzyMajaOwoce
        {
            get { return czyMajaOwoce; }
            set { czyMajaOwoce = value; }
        }

        public lisciaste()
        { }

        public lisciaste(string nazwa, int maxWiek, string przeznaczenie, double wysokosc, string czyMajaOwoce) 
              :base(nazwa, maxWiek, przeznaczenie, wysokosc)
        {
            this.CzyMajaOwoce = czyMajaOwoce;
        }

        public override string opisDrzewa()
        {
            return "A";
        }
    }
}
