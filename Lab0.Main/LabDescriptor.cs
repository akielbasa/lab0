﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(drzewo);
        public static Type B = typeof(iglaste);
        public static Type C = typeof(lisciaste);

        public static string commonMethodName = "opisDrzewa";
    }
}
