﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class iglaste : drzewo
    {
        string czyopadajaIgly;
        public string CzyOpadajaIgly
        {
            get { return czyopadajaIgly; }
            set { czyopadajaIgly = value; }
        }

        public iglaste()
        { }

        public iglaste(string nazwa, int maxWiek, string przeznaczenie, double wysokosc, string czyopadajaIgly) 
              :base(nazwa, maxWiek, przeznaczenie, wysokosc)
        {
            this.CzyOpadajaIgly = czyopadajaIgly;
        }
        public override string opisDrzewa()
        {
            return "B";
        }
    }
}
