﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class drzewo
    {
        string nazwa;
        int maxWiek;
        string przeznaczenie;
        double wysokosc;
        public string Nazwa { 
            get { return nazwa; }
            set { nazwa = value; } 
        }

        public int MaxWiek{
            get { return maxWiek; }
            set { maxWiek = value; }
    }
        public string Przeznaczenie
        {
            get { return przeznaczenie; }
            set { przeznaczenie = value; }
        }

        public double Wysokosc
        {
            get { return wysokosc; }
            set { wysokosc = value; }
        }

        public drzewo(string nazwa, int maxWiek, string przeznaczenie, double wysokosc)
        {
            this.Nazwa = nazwa;
            this.MaxWiek = maxWiek;
            this.Przeznaczenie = przeznaczenie;
            this.Wysokosc = wysokosc;
        }

        public drzewo()
        { }

        public virtual string opisDrzewa()
        {
            return "bb";
        }
    }
}
